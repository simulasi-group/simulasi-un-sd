﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DataUser : MonoBehaviour {

    private static DataUser instance = null;

    public List<MyAnswer> answer = new List<MyAnswer>(1);
    public string[] jawabanMapel1, jawabanMapel2, jawabanMapel3, jawabanMapel4, jawabanMapel5, jawabanMapel6;
    public string[] mapel;
    public float[] benarMapel;

    public static DataUser Instance
    {
        get { return instance; }
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    public void newLengthAnswer(int count)
    {
        for (int i = 0; i < count-1; i++)
        {
            var arr = new MyAnswer();
            answer.Add(arr);
        }
    }
}