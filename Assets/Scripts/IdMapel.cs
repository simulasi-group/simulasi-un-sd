﻿using UnityEngine;
using System.Collections;

public class IdMapel : MonoBehaviour {

    public int id;
    public string mapel;

	public void StartUN()
    {
        GameObject.Find("Mapel").GetComponent<ButtonScript>().StartUN(id);
    }

    public void Review()
    {
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Review>().ShowReviewData(mapel);
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Review>().SetMapel(id);
    }
}
