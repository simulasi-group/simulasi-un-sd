﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class RataRata : MonoBehaviour {

    float totalNilai;
    public GameObject background;
    public RectTransform vLayout;
    DataUser user;
    public Text noData;

    void Start ()
    {
        user = GameObject.FindGameObjectWithTag("DataUser").GetComponent<DataUser>();

        vLayout.sizeDelta = new Vector2(vLayout.sizeDelta.x, (background.GetComponent<RectTransform>().sizeDelta.y * (user.mapel.Length + 2)) +
            (vLayout.GetComponent<VerticalLayoutGroup>().spacing * (user.mapel.Length - 1 + 2)));

        //float[] nilai =
        //{
        //    (user.jawabanMapel1.Length > 0 ? (user.benarMapel[0] * ((float)100 / user.jawabanMapel1.Length)) : 0),
        //    (user.jawabanMapel2.Length > 0 ? (user.benarMapel[1] * ((float)100 / user.jawabanMapel2.Length)) : 0),
        //    (user.jawabanMapel6.Length > 0 ? (user.benarMapel[5] * ((float)100 / user.jawabanMapel6.Length)) : 0),
        //    (user.jawabanMapel3.Length > 0 ? (user.benarMapel[2] * ((float)100 / user.jawabanMapel3.Length)) : 0),
        //    (user.jawabanMapel4.Length > 0 ? (user.benarMapel[3] * ((float)100 / user.jawabanMapel4.Length)) : 0),
        //    (user.jawabanMapel5.Length > 0 ? (user.benarMapel[4] * ((float)100 / user.jawabanMapel5.Length)) : 0)
        //};
        if (user.benarMapel.Length > 0)
        {
            Destroy(noData);
            float[] nilai = new float[user.benarMapel.Length];

            for (int i = 0; i < user.benarMapel.Length; i++)
            {
                nilai[i] = user.benarMapel[i] * (user.answer[i].jawaban.Count > 0 ? ((float)100 / user.answer[i].jawaban.Count) : 0);
                totalNilai += nilai[i];
            }

            //totalNilai += nilai[i];

            for (int i = 0; i < user.benarMapel.Length; i++)
            {
                var hasil = "Nilai US/M " + user.mapel[i];
                HasilMapel(background, hasil, ": " + nilai[i]);
            }

            //mapel1.text = "Nilai UN Bahasa Indonesia\t\t\t\t\t\t\t: " + (user.jawabanMapel1.Length > 0 ? (user.benarMapel[0] * ((float)100 / user.jawabanMapel1.Length)).ToString() : "0");
            //mapel2.text = "Nilai UN Bahasa Inggris\t\t\t\t\t\t\t\t\t: " + (user.jawabanMapel2.Length > 0 ? (user.benarMapel[1] * ((float)100 / user.jawabanMapel2.Length)).ToString() : "0");
            //mapel3.text = "Nilai UN Matematika\t\t\t\t\t\t\t\t\t\t: " + (user.jawabanMapel6.Length > 0 ? (user.benarMapel[5] * ((float)100 / user.jawabanMapel6.Length)).ToString() : "0");
            //mapel4.text = "Nilai UN Kimia\t\t\t\t\t\t\t\t\t\t\t\t\t: " + (user.jawabanMapel3.Length > 0 ? (user.benarMapel[2] * ((float)100 / user.jawabanMapel3.Length)).ToString() : "0");
            //mapel5.text = "Nilai UN Fisika\t\t\t\t\t\t\t\t\t\t\t\t\t: " + (user.jawabanMapel4.Length > 0 ? (user.benarMapel[3] * ((float)100 / user.jawabanMapel4.Length)).ToString() : "0");
            //mapel6.text = "Nilai UN Biologi\t\t\t\t\t\t\t\t\t\t\t\t: " + (user.jawabanMapel5.Length > 0 ? (user.benarMapel[4] * ((float)100 / user.jawabanMapel5.Length)).ToString() : "0");

            var rata2 = Math.Round((totalNilai / user.benarMapel.Length), 2);
            string rataRata = "NILAI RATA - RATA US/M";
            HasilMapel(background, rataRata, ": " + rata2.ToString());

            string kategori = "<b>KATEGORI NILAI</b>";
            string nilaiKategori;
            if (rata2 > 85)
                nilaiKategori = "<b>: \"SANGAT BAIK\"</b>";
            else if (rata2 > 70 && rata2 <= 85)
                nilaiKategori = "<b>: \"BAIK\"</b>";
            else if (rata2 > 55 && rata2 <= 70)
                nilaiKategori = "<b>: \"CUKUP\"</b>";
            else
                nilaiKategori = "<b>: \"KURANG\"</b>";
            HasilMapel(background, kategori, nilaiKategori);
        }
    }

    void HasilMapel(GameObject background, string hasil, string nilai)
    {
        var newButton = Instantiate(background, vLayout.localPosition, Quaternion.identity) as GameObject;
        newButton.transform.parent = vLayout.transform;
        newButton.transform.localScale = new Vector3(1, 1, 1);
        newButton.transform.GetChild(0).GetComponent<Text>().text = hasil;
        newButton.transform.GetChild(1).GetComponent<Text>().text = nilai;
    }
}
